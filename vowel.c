#include<stdio.h>
#include<math.h>
#include<string.h>
void input(char *ch)
{
    printf("Enter a character");
    scanf("%c",ch);
}
void compute(char ch,int *result)
{
    if((ch=='A')||(ch=='E')||(ch=='I')||(ch=='O')||(ch=='U')||(ch=='a')||(ch=='e')||(ch=='i')||
    (ch=='o')||(ch=='u'))
    {
        *result=1;
    }
    else
        *result=0;
}
void output(char ch,int result)
{
    if(result==1)
    {
        printf("%c is a vowel",ch);
    }
    else
        printf("%c is a consonant",ch);
}
int main()
{
    char c;
    int r;
    input(&c);
    compute(c,&r);
    output(c,r);
    return 0;
}